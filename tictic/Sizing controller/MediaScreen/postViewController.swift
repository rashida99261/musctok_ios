//
//  postViewController.swift
//  TIK TIK
//
//  Created by Junaid  Kamoka on 28/08/2020.
//  Copyright © 2020 Junaid Kamoka. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import Photos

class postViewController: UIViewController,UITextViewDelegate {
    
    var videoUrl:URL?
    @IBOutlet weak var privacyView: UIView!
    
    @IBOutlet weak var viewStepper: UIView!
    @IBOutlet weak var publicLabel: UILabel!
    @IBOutlet weak var privacyIconImg: UIImageView!
    @IBOutlet weak var vidThumbnail: UIImageView!
    @IBOutlet weak var hasPartsSwitch : UISwitch!
   
    @IBOutlet weak var describeTextView: AttrTextView!
    
    @IBOutlet weak var txtLink: UITextField!
    @IBOutlet weak var txtParts: UITextField!
    
    
    @IBOutlet weak var viewFirstPart: UIView!
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var arrParts = NSMutableArray()
    
    var stepperCount = 1
    var privacyType = "Public"
    var desc = ""
    var allowDuet = "1"
    var allowComments = "true"
    var duet = "1"
    var soundId = "null"
    var saveV = "1"
    
    var hasParts = "0"
    var whichParts = ""
    var partsHashtag = ""
    
    var boxView = UIView()
    var blurView = UIView()
    
    var newVideoUrl:URL?
    
    var iPlayFrom = ""
    var imgMusic : UIImage?
    
    var isEditPage = ""
    var Vobj : videoMainMVC?
    
    
    var pickerParts: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        stepper.isHidden = true
        
        let privayOpt = UITapGestureRecognizer(target: self, action:  #selector(self.privacyOptionsList))
        self.privacyView.addGestureRecognizer(privayOpt)
        
        if(self.iPlayFrom == "audio"){
            self.vidThumbnail.image = imgMusic
        }
        else if(self.iPlayFrom == "video"){
            self.getThumbnailImageFromVideoUrl(url: videoUrl!) { (thumb) in
                self.vidThumbnail.image = thumb
            }
        }
        
        let dict_2 = ["name": "Part 2", "value": "2"]
        let dict_3 = ["name": "Part 3", "value": "3"]
        let dict_4 = ["name": "Part 4", "value": "4"]
        let dict_5 = ["name": "Part 5", "value": "5"]
        let dict_6 = ["name": "Part 6", "value": "6"]
        let dict_7 = ["name": "Part 7", "value": "7"]
        let dict_8 = ["name": "Part 8", "value": "8"]
        let dict_9 = ["name": "Part 9", "value": "9"]
        let dict_10 = ["name": "Part 10", "value": "10"]
        
        self.arrParts.add(dict_2)
        self.arrParts.add(dict_3)
        self.arrParts.add(dict_4)
        self.arrParts.add(dict_5)
        self.arrParts.add(dict_6)
        self.arrParts.add(dict_7)
        self.arrParts.add(dict_8)
        self.arrParts.add(dict_9)
        self.arrParts.add(dict_10)
        
        if(self.isEditPage == "edit"){
            
            //set editData here
            let desc = Vobj?.description
            
            print(Vobj)
            
            describeTextView.text = desc ?? "Describe your video"
            describeTextView.textColor = UIColor.black
            
            if let hasparts = Vobj?.has_parts{
                
                if(hasparts == "" || hasparts == "0"){
                    self.hasParts = "0"
                    hasPartsSwitch.isOn = false
                    viewStepper.isHidden = true
                    self.viewFirstPart.isHidden = true
                    
                    
                    self.btnYes.isSelected = false
                    self.btnNo.isSelected = false

                    
                }else{
                    self.hasParts = "1"
                    hasPartsSwitch.isOn = true
                    
                    let which_part = Vobj?.which_part
                    
                    if(which_part == ""){
                        viewStepper.isHidden = true
                        self.viewFirstPart.isHidden = true
                        self.btnYes.isSelected = false
                        self.btnNo.isSelected = false
                        
                        self.btnYes.isUserInteractionEnabled = true
                        self.btnNo.isUserInteractionEnabled = true

                    }

                    else if(which_part == "1"){
                        
                        viewStepper.isHidden = true
                        self.viewFirstPart.isHidden = false
                        self.btnYes.isSelected = true
                        self.btnNo.isSelected = false

                        self.btnYes.isUserInteractionEnabled = false
                        self.btnNo.isUserInteractionEnabled = false
                    }
                    else{
                        
                        viewStepper.isHidden = false
                        self.viewFirstPart.isHidden = false
                        self.btnYes.isSelected = false
                        self.btnNo.isSelected = true
                        
                        self.btnYes.isUserInteractionEnabled = true
                        self.btnNo.isUserInteractionEnabled = true

                    }
                }
            }
        }
        else{
            
            describeTextView.text = "Describe your video"
            describeTextView.textColor = UIColor.lightGray
            
            viewStepper.isHidden = true
            hasPartsSwitch.isOn = false
            self.viewFirstPart.isHidden = true
            
            self.btnYes.isSelected = false
            self.btnNo.isSelected = false

        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.lightGray
            textView.text = .none
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Describe your video"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if #available(iOS 13.0, *) {
            describeTextView.setText(text: describeTextView.text,textColor: .black, withHashtagColor: #colorLiteral(red: 1, green: 0.2705882353, blue: 0.2274509804, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
                print("type: ",type)
                print("strng: ",strng)
            }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
        } else {
            describeTextView.setText(text: describeTextView.text,textColor: .white, withHashtagColor: #colorLiteral(red: 1, green: 0.2705882353, blue: 0.2274509804, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
                print("type: ",type)
                print("strng: ",strng)
            }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
            // Fallback on earlier versions
        }
    }
    
    func uploadData(){
        
        AppUtility?.startLoader(view: self.view)
        if(UserDefaults.standard.string(forKey: "sid") == nil || UserDefaults.standard.string(forKey: "sid") == ""){
            
            UserDefaults.standard.set("null", forKey: "sid")
        }
        
        let url : String = ApiHandler.sharedInstance.baseApiPath+"postVideo"
        
        
        
        let cmnt = self.allowComments
        let allwDuet = self.allowDuet
        let prv = self.privacyType
        var des = self.desc
        
        
        
        if describeTextView.text != "Describe your video" {
            des = describeTextView.text
        }else{
            des = ""
        }
        
        let strHashtag = self.txtLink.text!
        let strArr = strHashtag.components(separatedBy: "?")
        self.partsHashtag = strArr.last ?? ""

        
        let parameter :[String:Any]? = ["user_id":UserDefaults.standard.string(forKey: "userID")!,
                                        "sound_id":"null",
                                        "description":des,
                                        "privacy_type":prv,
                                        "allow_comments":cmnt,
                                        "allow_duet":allwDuet,
                                        "has_parts": self.hasParts,
                                        "which_part": self.whichParts,
                                        "parts_hashtag": self.partsHashtag]
        print(parameter)
        
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        
        let serializer = DataResponseSerializer(emptyResponseCodes: Set([200, 204, 205]))
        
        AF.upload(multipartFormData: { MultipartFormData in
            for (key,value) in parameter! {
                MultipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            
            print("video url:-",self.videoUrl)
            
            do{
                let vid = try NSData(contentsOf: self.videoUrl!, options: .alwaysMapped)
                MultipartFormData.append(self.videoUrl!, withName: "video")
            }catch{
                print("catch err: ",error)
            }
            
        }, to: url, method: .post, headers: headers)
        .responseJSON { (response) in
            //                HomeViewController.removeSpinner(spinner: sv)
            //                AppUtility?.stopLoader(view: self.view)
            switch response.result{
            
            case .success(let value):
                
                print("progress: ",Progress.current())
                let json = value
                let dic = json as! NSDictionary
                
                print("response:- ",response)
                if(dic["code"] as! NSNumber == 200){
                    print("200")
                    debugPrint("SUCCESS RESPONSE: \(response)")
                    
                    if self.saveV == "1"{
                        self.saveVideoToAlbum(self.videoUrl!) { (err) in
                            
                            if err != nil{
                                //                                    AppUtility?.stopLoader(view: self.view)
                                print("Unable to save video to album dur to: ",err!)
                                self.showToast(message: "Unable to save video to album dur to:", font: .systemFont(ofSize: 12))
                            }else{
                                //                                    HomeViewController.removeSpinner(spinner: sv)
                                
                                print("video saved to gallery")
                                self.showToast(message: "video saved to gallery", font: .systemFont(ofSize: 12))
                            }
                            
                        }
                    }
                    AppUtility?.stopLoader(view: self.view)
                    print("Dict: ",dic)
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    // })
                }else{
                    AppUtility?.stopLoader(view: self.view)
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    print(dic)
                    
                }
            case .failure(let error):
                print("\n\n===========Error===========")
                print("Error Code: \(error._code)")
                print("Error Messsage: \(error.localizedDescription)")
                if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                    print("Server Error: " + str)
                }
                debugPrint(error as Any)
                print("===========================\n\n")
            }
            
        }
        .response(responseSerializer: serializer) { response in
            // Process response.
            switch response.result{
            case .success(let value):
                let json = value
                let dic = json as? NSDictionary
                let code = dic?["code"] as? NSString
                
                print("value: ",value)
                print("response",response)
                if(code == "200"){
                    print("200")
                    debugPrint("SUCCESS RESPONSE: \(response)")
                }else{
                    print("dic: ",dic)
                }
                
                if self.saveV == "1"{
                    self.saveVideoToAlbum(self.videoUrl!) { (err) in
                        
                        if err != nil{
                            //                                    AppUtility?.stopLoader(view: self.view)
                            print("Unable to save video to album dur to: ",err!)
                            self.showToast(message: "Unable to save video to album dur to:", font: .systemFont(ofSize: 12))
                            AppUtility?.stopLoader(view: self.view)
                        }else{
                            //                                    HomeViewController.removeSpinner(spinner: sv)
                            
                            print("video saved to gallery")
                            self.view.window?.rootViewController?.dismiss(animated: false, completion: nil)
                            AppUtility?.stopLoader(view: self.view)
                            //                            self.showToast(message: "video saved to gallery", font: .systemFont(ofSize: 12))
                        }
                    }
                }
            case .failure(let error):
                print("\n\n===========Error===========")
                print("Error Code: \(error._code)")
                print("Error Messsage: \(error.localizedDescription)")
                if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                    print("Server Error: " + str)
                }
                debugPrint(error as Any)
                print("===========================\n\n")
            }
        }
    }
    
    @IBAction func btnPost(_ sender: Any)
    {
        if(self.isEditPage == "edit"){
            self.uploadDataWithEdit()
        }
        else{
            self.uploadData()
        }
    }
    
    @IBAction func commentSwitch(_ sender: UISwitch) {
        if sender.isOn {
            self.allowComments = "true"
        }else{
            self.allowComments = "false"
        }
    }
    
    @IBAction func duetSwitch(_ sender: UISwitch) {
        if sender.isOn {
            self.allowDuet = "1"
        }else{
            self.allowDuet = "0"
        }
    }
    
    @IBAction func saveSwitch(_ sender: UISwitch) {
        if sender.isOn {
            self.saveV = "1"
        }else{
            self.saveV = "0"
        }
        
    }
    
    @IBAction func hasPartsSwitch(_ sender: UISwitch) {
        if sender.isOn{
            self.hasParts = "1"
            self.viewFirstPart.isHidden = false
            
        }
        else{
            self.hasParts = "0"
            self.viewFirstPart.isHidden = true
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
        print("pressed")
    }
    
    //    MARK:- VIEWWILL APPEAR
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("selected row : ",UserDefaults.standard.integer(forKey: "selectRow"))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("privTypeNC"), object: nil)
        
    }
    
    //    MARK:- UIVIEWS ACTIONS
    @objc func privacyOptionsList(sender : UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "privacyVC") as! privacyViewController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
        
    }
    
    //    MARK:- CHANEGE PRIVACY INFO
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        if let type = notification.userInfo?["privType"] as? String {
            print("type: ",type)
            self.publicLabel.text = type
            self.privacyType = type
            
            switch type {
            case "Public":
                self.privacyIconImg.image = #imageLiteral(resourceName: "openLockIcon")
                UserDefaults.standard.set(0, forKey: "selectRow")
            case "Friends":
                self.privacyIconImg.image = #imageLiteral(resourceName: "30")
            case "Private":
                self.privacyIconImg.image = #imageLiteral(resourceName: "lockedLockIcon")
                UserDefaults.standard.set(1, forKey: "selectRow")
            default:
                self.privacyIconImg.image = #imageLiteral(resourceName: "openLockIcon")
                self.publicLabel.text = "Public"
                self.privacyType = "Public"
                UserDefaults.standard.set(0, forKey: "selectRow")
            //                UserDefaults.standard.set(0, forKey: "selection")
            }
        }
    }
    
    
    //    MARK:- SET VIDEO THUMBNAIL FUNC
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbNailImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
    //    MARK:- SAVE VIDEO DATA
    
    func requestAuthorization(completion: @escaping ()->Void) {
        if PHPhotoLibrary.authorizationStatus() == .notDetermined {
            PHPhotoLibrary.requestAuthorization { (status) in
                DispatchQueue.main.async {
                    completion()
                }
            }
        } else if PHPhotoLibrary.authorizationStatus() == .authorized{
            completion()
        }
    }
    
    
    
    func saveVideoToAlbum(_ outputURL: URL, _ completion: ((Error?) -> Void)?) {
        requestAuthorization {
            PHPhotoLibrary.shared().performChanges({
                let request = PHAssetCreationRequest.forAsset()
                request.addResource(with: .video, fileURL: outputURL, options: nil)
            }) { (result, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        print("Saved VIDEO TO photos successfully")
                    }
                    completion?(error)
                }
            }
        }
    }
    
    func showShimmer(progress: String){
        
        //        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 70, width: 180, height: 50))
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = progress
        
        blurView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.3)
        let blurView = UIView(frame: UIScreen.main.bounds)
        
        
        boxView.addSubview(blurView)
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        
        view.addSubview(boxView)
    }
    
    func HideShimmer(){
        boxView.removeFromSuperview()
    }
    
    
    //    MARK:- BTN HASHTAG AND MENTIONS SETUPS
    @IBAction func btnHashtag(_ sender: UISwitch) {
        
        guard self.describeTextView.text != "Describe your video" else {return}
        
        self.describeTextView.setText(text: describeTextView.text+" #",textColor: .black, withHashtagColor: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
            print("type: ",type)
            print("strng: ",strng)
        }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
        
    }
    
    @IBAction func btnMention(_ sender: UISwitch) {
        
        guard self.describeTextView.text != "Describe your video" else {return}
        
        self.describeTextView.setText(text: describeTextView.text+" @",textColor: .black, withHashtagColor: #colorLiteral(red: 1, green: 0.2705882353, blue: 0.2274509804, alpha: 1), andMentionColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), andCallBack: { (strng, type) in
            print("type: ",type)
            print("strng: ",strng)
        }, normalFont: .systemFont(ofSize: 14, weight: UIFont.Weight.light), hashTagFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold), mentionFont: .systemFont(ofSize: 14, weight: UIFont.Weight.bold))
    }
}

extension postViewController {
    
    
    @IBAction func clickOnBtnSelectPart(_ sender: UIButton) {
        
        if(sender.tag == 10){
            
            self.whichParts = "1"
            self.btnYes.isSelected = true
            self.btnNo.isSelected = false
            self.viewStepper.isHidden = true
        }
        else if(sender.tag == 20){
            
            //  self.whichParts = "1"
            
            self.btnYes.isSelected = false
            self.btnNo.isSelected = true
            self.viewStepper.isHidden = false
        }
    }
}

extension postViewController {
    
    func encodeVideo(at videoURL: URL, completionHandler: ((URL?, Error?) -> Void)?)  {
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        
        let startDate = Date()
        
        //Create Export session
        guard let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough) else {
            completionHandler?(nil, nil)
            return
        }
        
        //Creating temp path to save the converted video
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
        
        let filePath = documentsDirectory.appendingPathComponent("rendered-Video.mp4")
        
        print(filePath)
        
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.removeItem(at: filePath)
            } catch {
                completionHandler?(nil, error)
            }
        }
        
        exportSession.outputURL = filePath
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
        let range = CMTimeRangeMake(start: start, duration: avAsset.duration)
        exportSession.timeRange = range
        
        exportSession.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession.status {
            case .failed:
                print(exportSession.error ?? "NO ERROR")
                completionHandler?(nil, exportSession.error)
            case .cancelled:
                print("Export canceled")
                completionHandler?(nil, nil)
            case .completed:
                //Video conversion finished
                let endDate = Date()
                
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful!")
                print(exportSession.outputURL ?? "NO OUTPUT URL")
                completionHandler?(exportSession.outputURL, nil)
                
            default: break
            }
            
        })
    }
}

extension postViewController {
    
    
    func uploadDataWithEdit(){
        
        AppUtility?.startLoader(view: self.view)
        if(UserDefaults.standard.string(forKey: "sid") == nil || UserDefaults.standard.string(forKey: "sid") == ""){
            
            UserDefaults.standard.set("null", forKey: "sid")
        }
        
        let url : String = ApiHandler.sharedInstance.baseApiPath+"updateVideoDetail"
        
        let cmnt = self.allowComments
        let allwDuet = self.allowDuet
        let prv = self.privacyType
        var des = self.desc
        
        if describeTextView.text != "Describe your video" {
            des = describeTextView.text
        }else{
            des = ""
        }
        
        let strHashtag = self.txtLink.text!
        let strArr = strHashtag.components(separatedBy: "?")
        self.partsHashtag = strArr.last ?? ""
        
        let parameter :[String:Any]? = ["user_id":UserDefaults.standard.string(forKey: "userID")!,
                                        "sound_id":"null",
                                        "description":des,
                                        "privacy_type":prv,
                                        "allow_comments":cmnt,
                                        "video_id": self.Vobj?.videoID ?? "",
                                        "allow_duet":allwDuet,
                                        "has_parts": self.hasParts,
                                        "which_part": self.whichParts,
                                        "parts_hashtag": self.partsHashtag]
        
        print(url)
        print(parameter!)
        
        let headers: HTTPHeaders = [
            "api-key": API_KEY
        ]
        
        AF.request(url, method: .post, parameters: parameter, encoding:JSONEncoding.default, headers:headers).validate().responseJSON(completionHandler: {
            
            respones in
            
            switch respones.result {
            case .success( let value):
                
                let json = value
                print(json)
                let dic = json as! NSDictionary
                let code = dic["code"] as! Int
                
                if(code == 200){
                    AppUtility?.stopLoader(view: self.view)
                    print("Dict: ",dic)
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                }else{
                    AppUtility?.stopLoader(view: self.view)
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                    print(dic)
                }
            case .failure(let error):
                print("failure err",error)
            }
        })
    }
}



extension postViewController : UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //  if component == 0 {
        if pickerView == pickerParts{
            return self.arrParts.count
        }
        
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerParts{
            
            let dict = self.arrParts[row] as! NSDictionary
            let strTitle = dict["name"] as! String
            return strTitle
        }
        
        return ""
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        if(pickerView == pickerParts){
            let dict = self.arrParts[row] as! NSDictionary
            let strTitle = dict["name"] as! String
            self.whichParts = dict["value"] as! String
            self.txtParts.text = strTitle
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtParts){
            self.pickerParts = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerParts.delegate = self
            self.pickerParts.dataSource = self
            textField.inputView = self.pickerParts
        }
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(named: "blackAndWhite")
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtParts.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if(textField == self.txtParts){
            self.pickUp(txtParts)
        }
    }
}
