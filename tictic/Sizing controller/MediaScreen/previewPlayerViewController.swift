//
//  previewPlayerViewController.swift
//  TIK TIK
//
//  Created by Junaid  Kamoka on 22/08/2020.
//  Copyright © 2020 Junaid Kamoka. All rights reserved.
//

import UIKit
import Player
import Alamofire
import DSGradientProgressView

class previewPlayerViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var impMusicPlayer: UIImageView!
    
    var imgPass : UIImage?
    
    var url:URL?
    
    fileprivate var player = Player()
    
    @IBOutlet weak var playerView: AGVideoPlayerView!
    
    var iPlayFrom = ""
    var songName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        if(self.iPlayFrom == "audio"){
            self.impMusicPlayer.isHidden = false
            playerSetup()
        }
        else if(self.iPlayFrom == "video"){
            self.impMusicPlayer.isHidden = true
            playerSetup()
        }
    }
    
    
//    func playSound(_ soundName: String) {
//
//        if soundName.hasPrefix("file:///") { // soundName
//            let audioUrl = URL(string: soundName)
//            let documentsPath = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
//            let docUrl = documentsPath.appendingPathComponent(songName)
//            let destinationURL = docUrl.appendingPathComponent(audioUrl!.lastPathComponent)
//            self.url = destinationURL
//            playerSetup()
//
//        }
//        else{
//            playerSetup()
//        }
//    }
    
    func playerSetup(){
        
        playerView.videoUrl = url!
        playerView.shouldAutoplay = true
        playerView.shouldAutoRepeat = true
        playerView.showsCustomControls = false
        playerView.shouldSwitchToFullscreen = true
        
        if(self.iPlayFrom == "audio"){
            impMusicPlayer.image = imgPass
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "postVC") as! postViewController
        vc.videoUrl = url
        vc.iPlayFrom = self.iPlayFrom
        if(self.iPlayFrom == "audio"){
            vc.imgMusic = self.imgPass
        }
        vc.isEditPage = "add"
        vc.modalPresentationStyle = .fullScreen
        UserDefaults.standard.set("Public", forKey: "privOpt")
        self.present(vc, animated: true, completion: nil)
    }
    
    internal func saveVideo(withURL url: URL) {
        let  sv = HomeViewController.displaySpinner(onView: self.view)
        let imageData:NSData = NSData.init(contentsOf: url)!
        
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if(UserDefaults.standard.string(forKey: "sid") == nil || UserDefaults.standard.string(forKey: "sid") == ""){
            
            UserDefaults.standard.set("null", forKey: "sid")
        }
        
        let url : String = self.appDelegate.baseUrl!+self.appDelegate.uploadVideo!
        
        let parameter :[String:Any]? = ["fb_id":UserDefaults.standard.string(forKey: "uid")!,"videobase64":["file_data":strBase64],"sound_id":"null","description":"xyz","privacy_type":"Public","allow_comments":"true","allow_duet":"1","video_id":"009988"]
        
        print(url)
        print(parameter!)
        let headers: HTTPHeaders = [
            "api-key": "4444-3333-2222-1111"
            
        ]
        
        AF.request(url, method: .post, parameters: parameter, encoding:JSONEncoding.default, headers:headers).validate().responseJSON(completionHandler: {
            
            respones in
            
            switch respones.result {
            case .success( let value):
                
                let json  = value
                
                HomeViewController.removeSpinner(spinner: sv)
                print("json: ",json)
                let dic = json as! NSDictionary
                let code = dic["code"] as! NSString
                if(code == "200"){
                    
                    //                                UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
                    //                                // terminaing app in background
                    //                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    //exit(EXIT_SUCCESS)
                    print("Dict: ",dic)
                    self.dismiss(animated:true, completion: nil)
                    // })
                    
                }else{
                    
                    
                }
                
            case .failure(let error):
                HomeViewController.removeSpinner(spinner: sv)
                print(error)
            }
        })
        
    }
}
