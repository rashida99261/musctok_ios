//
//  musicViewController.swift
//  MusicTok
//
//  Created by Admin on 20/06/21.
//  Copyright © 2021 Junaid Kamoka. All rights reserved.
//

import UIKit
import MediaPlayer
import Photos


class musicViewController: UIViewController, UITabBarControllerDelegate {
    
    @IBOutlet weak var imageTick:UIImageView!
    var isSelectImage = false
    
    
    var imagePicker: RImagePicker!
    
    var selectImg : UIImage?
    
    var documentURL = { () -> URL in
        let documentURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return documentURL
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker = RImagePicker(presentationController: self, delegate: self)
        self.imageTick.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnBack(_ sender: UIButton){
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openMusicBtn(_ sender: UIButton){
        
        if(isSelectImage == false){
            let alertController = UIAlertController(title: "Alert", message: "Please select first image to upload music.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            self.presentMusicPicker(sender)
        }
        
    }
    @IBAction func openMusicImageBtn(_ sender : UIButton){
        self.imagePicker.present(from: sender)
    }
}


extension musicViewController {
    
    func checkForMusicLibraryAccess(andThen f:(()->())? = nil) {
        let status = MPMediaLibrary.authorizationStatus()
        switch status {
        case .authorized:
            f?()
        case .notDetermined:
            MPMediaLibrary.requestAuthorization() { status in
                if status == .authorized {
                    DispatchQueue.main.async {
                        f?()
                    }
                }
            }
        case .restricted:
            // do nothing
            break
        case .denied:
            // do nothing, or beg the user to authorize us in Settings
            break
        }
    }
    
    func presentMusicPicker (_ sender: Any) {
        checkForMusicLibraryAccess {
            let picker = MPMediaPickerController(mediaTypes:.music)
            picker.showsCloudItems = false
            picker.delegate = self
            picker.allowsPickingMultipleItems = true
            picker.modalPresentationStyle = .popover
            picker.preferredContentSize = CGSize(width: 500,height: 600)
            self.present(picker, animated: true)
            if let pop = picker.popoverPresentationController {
                if let b = sender as? UIBarButtonItem {
                    pop.barButtonItem = b
                }
            }
        }
    }
}


extension musicViewController : MPMediaPickerControllerDelegate {
    // must implement these, as there is no automatic dismissal
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        //   self.view.showBlurLoader()
        for tempItem in mediaItemCollection.items {
            
            var dict = [String:Any]()
            let item: MPMediaItem = tempItem
            let pathURL: URL? = item.value(forProperty: MPMediaItemPropertyAssetURL) as? URL
            if pathURL == nil {
                print("Picking Error")
                return
            }
            let songAsset = AVURLAsset(url: pathURL!, options: nil)
            
            let tracks = songAsset.tracks(withMediaType: .audio)
            
            if(tracks.count > 0){
                let track = tracks[0]
                if(track.formatDescriptions.count > 0){
                    let desc = track.formatDescriptions[0]
                    let audioDesc = CMAudioFormatDescriptionGetStreamBasicDescription(desc as! CMAudioFormatDescription)
                    let formatID = audioDesc?.pointee.mFormatID
                    
                    var fileType:NSString?
                    var ex:String?
                    
                    switch formatID {
                    case kAudioFormatLinearPCM:
                        print("wav or aif")
                        let flags = audioDesc?.pointee.mFormatFlags
                        if( (flags != nil) && flags == kAudioFormatFlagIsBigEndian ){
                            fileType = "public.aiff-audio"
                            ex = "aif"
                        }else{
                            fileType = "com.microsoft.waveform-audio"
                            ex = "wav"
                        }
                        
                    case kAudioFormatMPEGLayer3:
                        print("mp3")
                        fileType = "com.apple.quicktime-movie"
                        ex = "mp3"
                        break;
                        
                    case kAudioFormatMPEG4AAC:
                        print("m4a")
                        fileType = "com.apple.m4a-audio"
                        ex = "m4a"
                        break;
                        
                    case kAudioFormatAppleLossless:
                        print("m4a")
                        fileType = "com.apple.m4a-audio"
                        ex = "m4a"
                        break;
                        
                    default:
                        break;
                    }
                    let exportSession = AVAssetExportSession(asset: AVAsset(url: pathURL!), presetName: AVAssetExportPresetAppleM4A)
                    exportSession?.shouldOptimizeForNetworkUse = true
                    
                    exportSession?.outputFileType = AVFileType.m4a ;
                    
                    var fileName = item.value(forProperty: MPMediaItemPropertyTitle) as! String
                    var fileNameArr = NSArray()
                    fileNameArr = fileName.components(separatedBy: " ") as NSArray
                    fileName = fileNameArr.componentsJoined(by: "")
                    fileName = fileName.replacingOccurrences(of: ".", with: "")
                    
                    print("fileName -> \(fileName)")
                    
                    let outputURL = documentURL().appendingPathComponent("\(fileName).m4a")
                    print("OutURL->\(outputURL)")
                    
                    //                    print("fileSizeString->\(item.fileSizeString)")
                    //                    print("fileSize->\(item.fileSize)")
                    
                    //                    dict = [
                    //                        "fileType":fileType as Any,
                    //                        "extention":ex as Any,
                    //                        "title":fileName,
                    //                        "storagePath":outputURL,
                    //                        "size":item.fileSizeString,
                    //                        "isCloudItem":item.isCloudItem
                    //                    ]
                    //
                    //                    let namePredicate = NSPredicate(format: "title like %@",dict["title"] as! String);
                    
                    
                    //                    let filteredArray = arrFetchedMedia.filter { namePredicate.evaluate(with: $0) };
                    //                    print("names = ,\(filteredArray)");
                    //
                    //                    if(filteredArray.count == 0)
                    //                    {
                    //                        arrFetchedMedia.add(dict)
                    //                    }
                    
                    do {
                        try FileManager.default.removeItem(at: outputURL)
                    } catch let error as NSError {
                        print(error.debugDescription)
                    }
                    
                    exportSession?.outputURL = outputURL
                    exportSession?.exportAsynchronously(completionHandler: { () -> Void in
                        
                        if exportSession!.status == AVAssetExportSession.Status.completed  {
                            print("Export Successfull")
                            //                            self.fetchImportedBeat()
                            //                            let arrArchive = NSKeyedArchiver.archivedData(withRootObject: self.arrFetchedMedia) as NSData
                            //
                            //                            let defaults = UserDefaults.standard
                            //
                            //                            defaults.set(arrArchive, forKey: "MediaArray")
                            //                            print("arr => \(self.arrFetchedMedia)")
                            
                            DispatchQueue.main.async {
                                
                                self.dismiss(animated: true) {
                                    let vc =  self.storyboard?.instantiateViewController(withIdentifier: "previewPlayerVC") as! previewPlayerViewController
                                    vc.url = outputURL
                                    vc.iPlayFrom = "audio"
                                    vc.imgPass = self.selectImg
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                }
                            }
                        } else {
                            // self.view.removeBluerLoader()
                            
                            self.dismiss(animated: true) {
                                print("Export failed")
                                print(exportSession!.error as Any)
                                
                            }
                        }
                    })
                }
            }
        }
        
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        print("cancel")
        self.dismiss(animated:true)
    }
    
    
    
}
extension musicViewController : RImagePickerDelegate{
    
    func didSelect(imgurl: URL?, image: UIImage?) {
        isSelectImage = true
        self.imageTick.isHidden = false
        self.selectImg = image
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }

    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}
