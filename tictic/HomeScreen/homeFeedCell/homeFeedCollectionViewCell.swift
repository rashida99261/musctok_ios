//
//  homeFeedCollectionViewCell.swift
//  TIK TIK
//
//  Created by Junaid  Kamoka on 01/10/2020.
//  Copyright © 2020 Junaid Kamoka. All rights reserved.
//

import UIKit
import GSPlayer
import MarqueeLabel
import DSGradientProgressView
import Lottie
import SnapKit
import AVFoundation

class homeFeedCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var playerView: VideoPlayerView!
    @IBOutlet weak var progressSlider: UISlider!
    
    @IBOutlet weak var btnLike: UIImageView!
    @IBOutlet weak var btnShare: UIImageView!
    @IBOutlet weak var btnComment: UIImageView!
    
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblTimeElapsed: UILabel!
    @IBOutlet weak var playerCD: UIImageView!
    
    @IBOutlet weak var txtDesc: AttrTextView!
    
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var verifiedUserImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var musicName: MarqueeLabel!
    
    @IBOutlet weak var optionsStackView: UIStackView!
    
    @IBOutlet weak var progressView: DSGradientProgressView!
    
    @IBOutlet weak var btnPlayImg: UIImageView!
    @IBOutlet weak var btnViewAllParts: UIButton!
    private var url: URL!
    
    @IBOutlet weak var btnFollow: UIButton!
    
    @IBOutlet weak var btnPrevPlay: UIButton!
    @IBOutlet weak var btnNextPlay: UIButton!
    
    var heartAnimationView : AnimationView?
    
    var isLiked = false
//    var timeElapsed : Float?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.txtDesc.textContainerInset = UIEdgeInsets.zero
        self.txtDesc.textContainer.lineFragmentPadding = 0
        
        btnPlayImg.isHidden = true
        
        playerView.contentMode = .scaleAspectFill
        
        userImg.makeRounded()
        playerView.stateDidChanged = { state in
            switch state {
            case .none:
                print("none")
            case .error(let error):
                
                print("error - \(error.localizedDescription)")
                self.progressView.wait()
                self.progressView.isHidden = false
                
                NotificationCenter.default.post(name: Notification.Name("errInPlay"), object: nil, userInfo: ["err":error.localizedDescription])
                
            case .loading:
                print("loading")
                self.progressView.wait()
                self.progressView.isHidden = false
            case .paused(let playing, let buffering):
                print("paused - progress \(Int(playing * 100))% buffering \(Int(buffering * 100))%")
                self.progressView.signal()
                self.progressView.isHidden = true
                self.playerCD.stopRotating()
            case .playing:
                self.btnPlayImg.isHidden = true
                self.progressView.isHidden = true
                self.playerCD.startRotating()
                
                print("playing")
            }
            
            switch state {
            case .playing, .paused:
                self.progressSlider.isEnabled = true
            default:
                self.progressSlider.isEnabled = false
            }
        }
        
        
        print(playerView)
        
        progressSlider.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapSlider(_:))))

    }
    
    @objc func tapSlider(_ gestureRecognizer: UIGestureRecognizer) {
        
        let pointTapped: CGPoint = gestureRecognizer.location(in: contentView)
        let positionOfSlider: CGPoint = progressSlider.frame.origin
        let widthOfSlider: CGFloat = progressSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(progressSlider.maximumValue) / widthOfSlider)
    
        progressSlider.setValue(Float(newValue), animated: false)
        sliderValueChanged(progressSlider)
        
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let time = CMTime(seconds: playerView.totalDuration * Double(sender.value), preferredTimescale: 60)
        playerView.seek(to: time)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        playerView.isHidden = true
    }
    
    func set(url: URL) {
        self.url = url
    }
    
    func play() {
        playerView.play(for: url)
        playerView.isHidden = false
        
        
        
        playerView.addPeriodicTimeObserver(forInterval: CMTime(seconds: 1, preferredTimescale: 60), using: { [weak self] _ in
            guard let self = self else { return }

            self.progressSlider.value = Float(self.playerView.currentDuration / self.playerView.totalDuration)
            let currentDuration = self.playerView.currentDuration
            self.lblTimeElapsed.text = self.getTimeString(seconds: Int(currentDuration))

        })

        
        
    }
    
    private func getTimeString(seconds: Int) -> String {
        String(format: "%02d:%02d", seconds / 60, seconds % 60)
    }
    
    func pause() {
        playerView.pause(reason: .hidden)
    }
    
    func like(){
        heartAnimationView?.backgroundColor = .clear
        heartAnimationView = .init(name: "lottieHeart")
        //                heartAnimationView?.frame = btnLike.frame
        heartAnimationView?.animationSpeed = 1
        //        heartAnimationView?.loopMode = .loop
        heartAnimationView?.sizeToFit()
        btnLike.addSubview(heartAnimationView!)
        
        heartAnimationView?.snp.makeConstraints({ (mkr) in
            mkr.center.equalTo(btnLike)
        })
        
        heartAnimationView?.play(fromFrame: 13, toFrame: 60, loopMode: .none, completion: { (bol) in
            //            self.heartAnimationView?.play(toFrame: 23)
        })
        isLiked = true
        
    }
    func unlike(){
        heartAnimationView?.backgroundColor = .clear
        
        heartAnimationView?.play(fromFrame: 60, toFrame: 13, loopMode: .none, completion: { (bol) in
            self.heartAnimationView?.removeFromSuperview()
        })
        isLiked = false
        
    }
    
    func alreadyLiked(){
        
        heartAnimationView?.removeFromSuperview()
        heartAnimationView?.backgroundColor = .clear
        heartAnimationView = .init(name: "lottieHeart")
        //        heartAnimationView?.frame = btnLike.frame
        
        //        heartAnimationView?.loopMode = .loop
        heartAnimationView?.sizeToFit()
        btnLike.addSubview(heartAnimationView!)
        
        heartAnimationView?.snp.makeConstraints({ (mkr) in
            mkr.center.equalTo(btnLike)
        })
        //self.heartAnimationView?.removeFromSuperview()
        self.heartAnimationView?.currentFrame = 60
        isLiked = true
        
    }
    
}
extension UIImageView {
    
    func makeRounded() {
        
        //        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        //        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
extension UIView {
    func startRotating(duration: Double = 3) {
        let kAnimationKey = "rotation"
        
        if self.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = duration
            animate.repeatCount = Float.infinity
            animate.fromValue = 0.0
            animate.toValue = Float(M_PI * 2.0)
            self.layer.add(animate, forKey: kAnimationKey)
        }
    }
    func stopRotating() {
        let kAnimationKey = "rotation"
        
        if self.layer.animation(forKey: kAnimationKey) != nil {
            self.layer.removeAnimation(forKey: kAnimationKey)
        }
    }
    
    
}


extension CMTime {
    var roundedSeconds: TimeInterval {
        return seconds.rounded()
    }
    var hours:  Int { return Int(roundedSeconds / 3600) }
    var minute: Int { return Int(roundedSeconds.truncatingRemainder(dividingBy: 3600) / 60) }
    var second: Int { return Int(roundedSeconds.truncatingRemainder(dividingBy: 60)) }
    var positionalTime: String {
        return hours > 0 ?
            String(format: "%d:%02d:%02d",
                   hours, minute, second) :
            String(format: "%02d:%02d",
                   minute, second)
    }
}
