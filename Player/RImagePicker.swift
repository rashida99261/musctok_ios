//
//  imagePicker.swift
//  MusicTok
//
//  Created by Admin on 26/06/21.
//  Copyright © 2021 Junaid Kamoka. All rights reserved.
//

import UIKit

public protocol RImagePickerDelegate: class {
    //func didSelect(image: UIImage?)
    func didSelect(imgurl: URL?, image: UIImage?)
}

open class RImagePicker: NSObject {

    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: RImagePickerDelegate?

    public init(presentationController: UIViewController, delegate: RImagePickerDelegate) {
        self.pickerController = UIImagePickerController()

        super.init()

        self.presentationController = presentationController
        self.delegate = delegate

        self.pickerController.delegate = self
        self.pickerController.allowsEditing = false
        self.pickerController.mediaTypes = ["public.image"]
    }

    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }

        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }

    public func present(from sourceView: UIView) {

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        if let action = self.action(for: .camera, title: "Take photo") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .photoLibrary, title: "Photo library") {
            alertController.addAction(action)
        }

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }

        self.presentationController?.present(alertController, animated: true)
    }

    private func pickerController(_ controller: UIImagePickerController, didSelect imh_url: URL?, image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        self.delegate?.didSelect(imgurl: imh_url, image: image)
    }
}

extension RImagePicker: UIImagePickerControllerDelegate {

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil, image: nil)
        
    }

    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

//        guard let image = info[.imageURL] as? URL else {
//            self.pickerController(picker, didSelect: nil, image: nil)
//        }
//        self.pickerController(picker, didSelect: image)
        
        guard let url_img = info[.imageURL] as? URL else {
           return self.pickerController(picker, didSelect: nil, image: nil)
        }
        
        guard let image = info[.originalImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil, image: nil)
        }
        
        self.pickerController(picker, didSelect: url_img, image: image)
    }
}

extension RImagePicker: UINavigationControllerDelegate {

}
